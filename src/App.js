// import axios from "axios";
// import { useEffect } from "react";

import "./App.css";
import Beranda from "./components/Beranda";
import Pencarian from "./components/Pencarian";
import Tentang from "./components/Tentang";
import NavigationBar from "./components/NavigationBar";
import { Routes, Route } from "react-router-dom";

function App() {
  // const kbbiAPi = async () => {
  //   try {
  //     const data = await axios.g            <img src="" alt="github" />et(
  //       "https://kbbi-api-zhirrr.vercel.app/api/kbbi?text=hitam"
  //     );

  //     console.log(data);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  // useEffect(() => {
  //   kbbiAPi();
  // }, []);

  return (
    <div className="App">
      <NavigationBar />

      <Routes>
        <Route path="/kbbi-daring/" element={<Beranda />} />
        <Route path="/kbbi-daring/pencarian" element={<Pencarian />} />
        <Route path="/kbbi-daring/tentang" element={<Tentang />} />
      </Routes>
    </div>
  )
}

export default App;
