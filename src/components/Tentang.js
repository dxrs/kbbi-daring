import { Container } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGithub } from "@fortawesome/free-brands-svg-icons";
import { faGitlab } from "@fortawesome/free-brands-svg-icons";

const Tentang = () => {
  return (
    <div className="tentang">
      <Container className="text-center justify-content-center align-items-center">
        <div className="header-text">TENTANG</div>
        <div className="description-text">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
          elementum convallis turpis non vestibulum. Sed sed urna et nisl
          tincidunt accumsan. Vestibulum dignissim mauris metus, vel fermentum
          urna pellentesque in. Praesent a libero tincidunt, eleifend ipsum a,
          elementum tellus. Suspendisse congue, nibh vel vehicula egestas,
          tortor urna venenatis orci, sit amet luctus odio nisi vel nulla.
        </div>
        <div className="social-media">
          <a href="https://gitlab.com/dxrs">
            <FontAwesomeIcon icon={faGitlab} />
          </a>
          <a href="https://github.com/dannrs">
            <FontAwesomeIcon icon={faGithub} />
          </a>
        </div>
      </Container>
    </div>
  );
};

export default Tentang;
