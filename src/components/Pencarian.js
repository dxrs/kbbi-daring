import React, { useState } from "react";
import JaroWinkler from "./JaroWinkler";
import { Button, Form, ToggleButton } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCode } from "@fortawesome/free-solid-svg-icons";

const Pencarian = () => {
  const [query, setQuery] = useState("");
  const [results, setResults] = useState([]);
  const [showDiv, setShowDiv] = useState(false);
  const [searchTime, setSearchTime] = useState(0);
  const [showMetrics, setShowMetrics] = useState(false);

  const handleClick = () => {
    setShowMetrics(!showMetrics);
  };

  const handleSearchSubmit = (event) => {
    event.preventDefault();

    // Make a request to the API to get the data
    const data = require("../data.json");
    const startTime = performance.now();

    // Search for the exact match
    const exactMatch = data.find(
      (item) => item.lema.toLowerCase() === query.toLowerCase()
    );
    if (exactMatch) {
      setResults([exactMatch]);
      return;
    }

    // If there is no exact match, find items with a high Jaro-Winkler Distance
    const jaroWinklerThreshold = 0.7;
    const jaroWinklerResults = data
      .map((item) => ({
        item,
        distance: JaroWinkler(item.lema.toLowerCase(), query.toLowerCase()),
      }))
      .filter((result) => result.distance > jaroWinklerThreshold)
      .sort((a, b) => b.distance - a.distance)
      .map((result) => result.item);

    const endTime = performance.now();
    const searchTime = (endTime - startTime) / 1000;
    const topThreeResults = jaroWinklerResults.slice(0, 3);
    setSearchTime(searchTime);
    setResults(topThreeResults);
  };

  return (
    <div className="pencarian">
      <div className="search-form">
        <Form onSubmit={handleSearchSubmit}>
          <Form.Group className="search-form-container d-flex justify-content-center">
            <Form.Control
              type="text"
              placeholder="Pencarian ..."
              value={query}
              onChange={(event) => setQuery(event.target.value)}
            />
            <Button
              onClick={() => setShowDiv(true)}
              className="search-button"
              variant="dark"
              type="submit"
            >
              Cari
            </Button>
            <ToggleButton
              type="checkbox"
              defaultChecked={showMetrics}
              onClick={handleClick}
              style={{
                backgroundColor: showMetrics ? "#212529" : "#f1f6f7",
                color: showMetrics ? "white" : "#212529",
                borderColor: "#212529",
              }}
            >
              <FontAwesomeIcon icon={faCode} />
            </ToggleButton>
          </Form.Group>
        </Form>
      </div>
      {showDiv && (
        <div className="result-container">
          {results.map((result) => {
            const distance =
              result.lema.toLowerCase() === query.toLowerCase()
                ? 1
                : JaroWinkler(result.lema.toLowerCase(), query.toLowerCase());
            return (
              <div key={result.lema}>
                <div className="search-result">{result.lema}</div>
                <div>{result.definisi}</div>
                {showMetrics && (
                  <>
                    <Button className="similarity-button" variant="dark">
                      Similarity: {distance}
                    </Button>
                    <Button className="process-time-button" variant="dark">
                      Time: {searchTime} seconds
                    </Button>
                  </>
                )}
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default Pencarian;
