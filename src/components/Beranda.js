import { Button, Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

const Beranda = () => {
  return (
    <div className="beranda">
      <Container className="text-center justify-content-center align-items-center">
        <Row>
          <Col>
            <div className="header-text">SELAMAT DATANG!</div>
            <div className="description-text">
              KBBI Daring adalah situs pencarian kata dalam Kamus Besar Bahasa
              Indonesia (KBBI)
            </div>
            <div className="navbar-links mt-3">
              <Button variant="dark">
                <Link to="/kbbi-daring/pencarian">Pencarian »</Link>
              </Button>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Beranda;
