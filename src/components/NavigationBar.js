import { Navbar, Nav, Container } from "react-bootstrap";
import { Link } from "react-router-dom";

const NavigationBar = () => {
  return (
    <div>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand className="navbar-brand"><Link to="/kbbi-daring/">KBBI DARING</Link></Navbar.Brand>
          <Nav className="navbar-links">
            <Nav.Link>
              <Link to="/kbbi-daring/">Beranda</Link>
            </Nav.Link>
            <Nav.Link>
              <Link to="/kbbi-daring/pencarian">Pencarian</Link>
            </Nav.Link>
            <Nav.Link>
              <Link to="/kbbi-daring/tentang">Tentang</Link>
            </Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    </div>
  );
};

export default NavigationBar;
